import java.util.Arrays;
import java.util.Scanner;

public class Homework {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива: ");
        int size = scanner.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            System.out.println("Введите элемент массива: ");
            array[i] = scanner.nextInt();
        }
        System.out.println("Введите число на проверку: ");
        int num = scanner.nextInt();
       System.out.println(IndexOfElement(array, num));

    }

    public static int IndexOfElement(int[] array, int num) {

        int i = 0;
        for (; i < array.length; i++)
            if (array[i] == num)
                break;
        if (i != array.length)
            return i;
        else
            return -1;
    }
}